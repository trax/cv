* # Omar Givernaud
* o.givernaud@gmail.com
* XP: +13 years
* Full Remote or relocation worldwide



## Skills

* Software Engineering:
  * Design/Architecture.
  * Tech lead.
  * Development: Python/C++.
  * QA/CI/CD.
  * Cyber Security Architecture.

* Management:
  * Project and team.
  * Pragmatic and Agile. 
  

## Experience


### Rhobs - CTO<br>
IT Engineering<br>
Present - 2020 December

RHObs, a French startup in Human Resources, empowers companies to derive high-value metrics from employee data, facilitating insightful comparisons with other organizations.

* Architected software and infrastructure, implementing the CEO's vision for a fully serverless platform.<br>
*Python, JavaScript, TypeScript, AWS CDK, Gateway, Lambda, Cognito, CloudWatch, IAM, MongoDB.*
* Led the hiring process for the technical team and managed their technical realizations.<br>
*Team management, recruitment.*



### [Bioserenity](https://www.bioserenity.com) - Tech Lead for the Datascience<br>
Medical IOT<br>
December 2023 - 2020 February

* Designed, developed, deployed, and managed two products: datalake and a computing service for medical data.<br>
  * Python, AWS (API Gateway, Cognito, CDK, S3, Lambda, ...), MongoDB, cybersecurity, 0 to production.*
* DevOps philosophy and best practice evangelist.<br>
  * DevOps, Git, security management, devs in production, data security.*
* Developed and monitored cloud infrastructure.<br>
  * Terraform, Ansible, Prometheus, Python, AWS.*
* Developed connectors for a distributed computation platform.<br>
  * Python, Kafka, GitLab-CI, Jenkins.*


### [Neurochain](https://neurochaintech.io) - Tech Lead, from Architecture to Production of Fully Functional Blockchain<br>
Blockchain<br>
2020 April - 2018 June

* Design and Architecture of a new blockchain protocol.<br>
*C++17, blockchain, architecture, mesh network, MongoDB.*
* Setup the development environment.<br>
*hunter, CMake, GitLab (git, pipeline CI, issues), unit test (ctest + gtest), docker.*
* Team management (4 people).<br>
*Agile, recruitment.*
* Delivered and maintained functional testnet.<br>
*DevOps, CI/CD, ansible, python, AWS, from end-to-end.*
* Community management.<br>
*AMA, telegram, youtube interviews, and tutorials.*


### [Cassiopae](https://cassiopae.com/) - DevOps<br>
Banking<br>
2018 November - 2018 June

* Svn to git migration, for hundreds of developers.<br>
*GitLab, soft skills.*
* Dockerisation of build service for Jenkins CI.<br>
*docker, Jenkins.*


### [Nexatech](http://nexatech.fr) - Tech Lead C/C++ / Ops<br>
Homeland Security<br>
2018 November - 2016 March

* Developed and troubleshot C/C++ high band-with network probes.<br>
*C/C++, CMake, libasan, gdb.*
* Reverse-engineered and implemented fast network protocol parsing.<br>
*C, Wireshark, tcpdump.*
* Set up Continuous Integration and non-regression tools, test automation, and coding rules.<br>
*CTest, Jenkins, lxc.*
* Deployed on-site and monitored the solution.<br>
*Linux internals, atop, customer services.*


### [Amazon Web Service](https://aws.com) - System Engineer RDS (Relational Database Service)<br>
Dublin Ireland<br>
2015 August - 2014 June

* Investigate and resolve high severity incidents on production instances.<br>
*Linux internals, Oracle, MySql, Postgres, SQLServer.*
* Developed internal tools for daily investigation and automation.<br>
*Ruby on rails, Oracle, Wt.*
* Deployed monitoring probes for an RDS Service.


### [Enyx](http://enyx.fr) - Team Manager / C++ Developer<br>
Hardware acceleration for high-frequency trading<br>
2014 May - 2012 August

* Software Team Manager for a 6 members team.<br>
*Scrum, QA, Continuous Integration, recruitment.*
* Architect and developer:  Market data feed handler for equities and derivatives.<br>
*C++, High-Performance computing, Low Latency.*
* System Administrator of Enyx infrastructure and Enyx private Cloud<br>
*Centos, Debian, Ubuntu, libvirt.*

### [Somone](http://somone.fr) - Tech Lead C/Monitoring<br>
IT Monitoring and Event Management<br>
2012 August - 2011 January

* Software architect and developer for monitoring applications
  * Optimised for network Nagios probe
  * Highly reliable Nagios integration in BEM (BMC Event Manager).<br>
*C, glib, ZeroMQ, high reliability, cross-platform (RHEL, Debian, AIX, Windows).*
* Contribution to Nagios 4 road map.<br>
*Scalability enhancement.*
* System Administrator of Somone infrastructure and Somone private Cloud<br>
*Centos, Debian, libvirt.*
* Virtual SI simulation (>1000 virtualized Linux on a desktop computer).<br>
*KVM, lxc, libvirt, Gentoo.*


### [Nanyang Polytechnic of Singapore](http://www.nyp.edu.sg) - Attachment<br>
2010 June - 2010 April

* Developed, VGA driver.<br>
*Verilog, Quartus.*
* Developed, RAM buffer on FPGA for image processing<br>
*Verilog, FPGA.*


### [ESIEE Paris](https://www.esiee.fr) - Intership<br>
Research and Development Department<br>
2008 October - 2008 September 

* Developed a videoconferencing platform for domicile hospitalization of Alzheimer's disease as part of a European research project about telemedicine (CompanionAble).<br>
*C, VOIP, SIP, Asterisk.*
* Publication: Platform - Enhanced visiophony solution to operating a Robot-Companion<br>
*CISSE 2008, IEEE.*


## Education 


2011 - Master of Sciences at [ESIEE Paris](https://www.esiee.fr) 
* Double specialization in Computer Sciences and Embedded Systems.

## Languages

* French native language.
* English Fluent.
* Arabic spoken.
* Spanish basic.


## Misc 

* Cooking.
* Road Cycling.
* Go (the game).

See original: `https://gitlab.com/trax/cv/-/blob/master/README.md`
